create database SDA;

create table Autor (
id integer,
numeAutor varchar(20),
prenumeAutor varchar(20),
loculNasterii varchar(20)
);

insert into Autor values(1, "Creanga", "Ion", "Botosani");
insert into Autor values(2, "Eminescu", "Mihai", "Iasi");

select * from Autor;

select numeAutor, loculNasterii from Autor;

update Autor set loculNasterii="Bucuresti" where id=2;

select * from Autor;

insert into Autor values(2, "Eminescu", "Mihai", "Iasi");
update Autor set loculNasterii="Bucuresti" where id=2;
select * from Autor;

delete from Autor where id=1;
select * from Autor;
