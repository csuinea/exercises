-- Inner Join
select Scriitor.nume, Carte.numeCarte from Scriitor inner join Carte
on Scriitor.id_scriitor = Carte.id_scriitor;

select * from Scriitor inner join Carte
on Scriitor.id_scriitor = Carte.id_scriitor;