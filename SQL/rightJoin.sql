-- Right Join

select Scriitor.nume, Carte.numeCarte from Scriitor right join Carte
on Scriitor.id_scriitor = Carte.id_scriitor;

select Scriitor.nume, Carte.numeCarte from Carte right join Scriitor
on Scriitor.id_scriitor = Carte.id_scriitor;
