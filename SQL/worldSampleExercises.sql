describe table city;

select * from City;
select * from Country;

select City.name, City.Population, Country.name, Country.continent
from City inner join Country on City.CountryCode = Country.Code;

select C.name, C.Population, Co.name, Co.continent
from City C inner join Country Co on C.CountryCode = Co.Code;

select City.id, City.name, Country.name, Country.population 
from City left join Country on City.CountryCode = Country.Code;


select population from (select City.id, City.name, Country.name, Country.population 
from City left join Country on City.CountryCode = Country.Code);