create table if not exists Movies(
movie_id integer unique auto_increment not null,
movie_name varchar(40) not null,
movie_type varchar(30) not null,
movie_rating tinyint not null,
primary key(movie_id, movie_name)
);

insert into Movies values (1, "Inception", "SF", 5);
insert into Movies values (null, "Star Wars", "SF", 2);
insert into Movies values (null, "Tenet", "SF", 4);

select * from Movies;

create table if not exists movies_v2 like Movies;
create table if not exists movies_v2 as select * from movies;
select * from movies_v2;


create table if not exists company(
company_id varchar(28) unique,
trade varchar(40) not null,
number_of_employees integer not null default 0
);

create table if not exists job_offer(
offer_id varchar(36) unique,
offer_title varchar(40) default " ",
offer_min_salary decimal default 5000,
offer_max_salary decimal default null
);

insert into company values (7, "Trading", 15000);
select * from company; 
insert into company values ("electronic_arts", "gaming", 15000);
select company_id from company where number_of_employees>10000;

