	-- Full Join
-- SELECT * FROM t1
-- LEFT JOIN t2 ON t1.id = t2.id
-- UNION ALL
-- SELECT * FROM t1
-- RIGHT JOIN t2 ON t1.id = t2.id

select * from Scriitor left join Carte on Scriitor.id_scriitor = Carte.id_scriitor
union all select * from Scriitor right join Carte on Scriitor.id_scriitor = Carte.id_scriitor;
