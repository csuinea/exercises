insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Balotă','Nicolae','1925-01-01','oras1','eseu literar',true,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Eminescu','Mihai','1850-01-01','oras1','eseu literar',false,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Creanga','Ion','1837-01-01','oras1','eseu literar',false,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Slavici','Ion','1848-01-01','oras1','eseu literar',false,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Iorga','Nicolae','1871-01-01','oras1','eseu literar',false,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Noica','Constantin','1909-01-01','oras1','eseu literar',true,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Plesu','Andrei','1948-01-01','oras1','eseu literar',true,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Coman','Otilia','1942-01-01','oras1','eseu literar',true,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Eliade','Mircea','1907-01-01','oras1','eseu literar',false,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Paleologu','Alexandru','1919-01-01','oras1','eseu literar',false,1);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan,id_carte) values ('Sora','Mihai','1916-01-01','oras1','eseu literar',true,1);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('fat frumos','descr1',45,'gama','fantezie',true);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('scrisoarea 1','descr2',24,'gama','fantezie',false);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('scrisoarea 2','descr3',432,'gama','fantezie',true);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('scrisoarea 3','descr4',21,'gama','fantezie',false);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('scrisoarea 4','descr5',213,'gama','fantezie',true);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('luceafarul','descr6',432,'gama','fantezie',false);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('floarea albastra','descr7',312,'gama','fantezie',true);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover) 
values ('lacul','descr8',312,'gama','fantezie',false);

select * from Scriitor;