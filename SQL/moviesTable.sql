insert into Movies values (1, "Inception", "SF", 5);
insert into Movies values (2, "Men in Black", "SF", 2);
insert into Movies values (3, "Star Wars", "SF", 3);
insert into Movies values (4, "Zodiac", "Thriller", 4);
insert into Movies values (5, "Romania Neimblanzita", "SF", 3);
insert into Movies values (6, "John Wick", "Action", 5);
insert into Movies values (7, "Conjuring", "Horror", 5);
insert into Movies values (8, "Avengers", "SF", 2);
insert into Movies values (9, "Shutter Island", "Mystery", 4);
insert into Movies values (10, "James Bond", "Action", 2);

select * from Movies;
select rating from Movies where rating=2;
select movieName from Movies where rating=2;
select * from Movies where rating=2;
select movieType from Movies where rating=5;
select movieName from Movies where rating=2 and movieType="Action";
select count(movieName) from Movies where movieType="SF";
select movieName from Movies where movieType="SF";
select movieName from Movies where movieType="SF" order by movieName desc;
select * from Movies order by movieId asc;
update Movies set movieName="Star Wars: Rise of Skywalker" where movieName="Star Wars";
select * from Movies limit 2;

delete from Movies where movieName = "Inception";
select min(rating) from Movies; 
select max(rating) from Movies; 
select avg(rating) from Movies;
select avg(rating) as "Media Rating-urilor" from Movies;
select movieName as "Nume Film" from Movies;

select movieType from Movies where movieName="Romania Neimblanzita";
update Movies set movieType="Documentary" where movieName="Romania Neimblanzita";
select movieType as "Genul Filmului" from Movies where movieName="Romania Neimblanzita";

select movieType from Movies limit 3;
select distinct movieType from Movies limit 3;
delete from Movies where rating=7;
delete from Movies where rating>7;
delete from Movies where rating between 2 and 5;
select * from Movies;

select movieName from Movies where rating = 5 order by movieName asc;
select movieName from Movies where rating = 5 order by movieName desc;
select movieName from Movies where rating = 5 order by movieName;