create table if not exists Angajat(
id integer auto_increment,
nume varchar(30) not null,
prenume varchar(30) not null,
adresa varchar(50) not null,
dataNasterii date not null,
oras varchar(30) not null default "Bucuresti",
primary key(id)
);

drop table Angajat;