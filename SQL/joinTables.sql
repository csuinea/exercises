-- drop table Scriitor;
-- drop table Carte;
create table if not exists Scriitor(
id_scriitor int auto_increment,
nume varchar(100) not null,
prenume varchar(100) not null,
dataNasterii date not null,
orasNastere varchar(25) default "Botosani",
genLiterar varchar(25) not null,
contemporan boolean not null,
primary key(id_scriitor)
);
create table if not exists Carte(
id_carte int auto_increment,
numeCarte varchar(250) not null,
descriere varchar(250) not null,
numarPagini int not null,
editura varchar(250) not null,
genLiterar varchar(100) not null,
hardcover boolean not null,
dataAparitiei date default '2020-01-01',
primary key(id_carte)
);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Balotă','Nicolae','1925-01-01','oras1','eseu literar',true);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Eminescu','Mihai','1850-01-01','oras1','eseu literar',false);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Creanga','Ion','1837-01-01','oras1','eseu literar',false);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Slavici','Ion','1848-01-01','oras1','eseu literar',false);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Iorga','Nicolae','1871-01-01','oras1','eseu literar',false);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Noica','Constantin','1909-01-01','oras1','eseu literar',true);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Plesu','Andrei','1948-01-01','oras1','eseu literar',true);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Coman','Otilia','1942-01-01','oras1','eseu literar',true);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Eliade','Mircea','1907-01-01','oras1','eseu literar',false);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Paleologu','Alexandru','1919-01-01','oras1','eseu literar',false);
insert into Scriitor(nume,prenume,dataNasterii,orasNastere,genLiterar,contemporan) values ('Sora','Mihai','1916-01-01','oras1','eseu literar',true);
ALTER TABLE Carte
ADD id_scriitor integer;
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('fat frumos','descr1',45,'gama','fantezie',true,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('scrisoarea 1','descr2',24,'gama','fantezie',false,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('scrisoarea 2','descr3',432,'gama','fantezie',true,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('scrisoarea 3','descr4',21,'gama','fantezie',false,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('scrisoarea 4','descr5',213,'gama','fantezie',true,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('luceafarul','descr6',432,'gama','fantezie',false,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('floarea albastra','descr7',312,'gama','fantezie',true,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('lacul','descr8',312,'gama','fantezie',false,2);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('Maitreyi','Carte de extaz senzorial',199,'cartex',' Fictional Autobiography',false,9);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('Treptele lumii sau calea către sine a lui Mihail Sadoveanu','descriere carte',250,'Cartea Românească',' biografie',false,10);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('Locuri comune','descriere carte locuri comune',250,'Universalia',' biografie',true,11);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('Dune','descriere carte Dune',250,'Rao',' biografie',true,7);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('Dune 2','descriere carte Dune',250,'Rao',' biografie',true,9);
insert into Carte(numeCarte,descriere,numarPagini,editura,genLiterar,hardcover,id_scriitor) 
values ('LOTR','descriere carte Dune',250,'Rao',' biografie',true,5);

 ALTER TABLE Carte
 ADD FOREIGN KEY (id_scriitor) REFERENCES Scriitor(id_scriitor);
