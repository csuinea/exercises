create table if not exists Movies(
movieId integer auto_increment unique,
movieName varchar(40),
movieType varchar(30),
rating tinyint
);

create table if not exists Autor(
id integer,
numeAutor varchar(30),
prenumeAutor varchar(30),
dataNasterii date,
loculNasterii varchar(50),
genLiterar varchar(20)
);

create table Carte(
ISBN integer,
numeCarte varchar(50),
descriereCarte varchar(30),
pretCarte double(5,2),
numarPagini integer, 
edituraCarte varchar(30), 
genLiterar varchar(20)
);