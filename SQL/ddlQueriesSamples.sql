-- DDL queries--

alter table Autor add primary key(id);
alter table Autor modify numeAutor varchar(30) not null;
alter table Autor alter loculNasterii set default "Brasov";
alter table Autor auto_increment = 1;
alter table Autor add email varchar(40);
alter table Autor add genLiterar varchar(40);
alter table Autor rename column genLiterar to gen;
alter table Autor rename to Scriitori;

drop table Scriitori;