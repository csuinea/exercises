//Javascript source code
console.log("This is script!");

// document.getElementById("save-button").addEventListener("click", addElement);
var parent = document.getElementById("table-body");

var addElement = function () {
  var name = document.getElementById("username").value;
  var lastName = document.getElementById("user-surname").value;
  console.log(name, lastName);
  var childRow = document.createElement("tr");
  var childName = document.createElement("td");
  var childLastName = document.createElement("td");
  childName.innerHTML = name;
  childLastName.innerHTML = lastName;
  childRow.appendChild(childName);
  childRow.appendChild(childLastName);
  parent.appendChild(childRow);
  document.getElementById("username").value = "";
  document.getElementById("user-surname").value = "";
};
