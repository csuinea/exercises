package test1;

import model.Doctor;
import model.Human;
import model.Programmer;

public class Executie2 {
    public static void main(String[] args) {
        Programmer programmer = new Programmer();
        Doctor doctor = new Doctor();
        Human civilian = new Human();

        civilian.setFirstName("John");
        civilian.setLastName("Doe");
        civilian.setAge(33);
        civilian.setEducationLevel("College");
        System.out.println(civilian.toString());

        doctor.setFirstName("Ion");
        doctor.setLastName("Popescu");
        doctor.setAge(40);
        doctor.setEducationLevel("PhD");
        doctor.setSpeciality("Neurologist");
        System.out.println(doctor.toString());

        programmer.setFirstName("Jean");
        programmer.setLastName("Valjean");
        programmer.setBaseSalary(1500);
        programmer.setAge(29);
        programmer.setEducationLevel("Degree");
        programmer.setTechnologyStack("Java");
        System.out.println(programmer.toString());
    }
}
