package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Human {
    private String firstName;
    private String lastName;
    private int age;
    private String educationLevel;


    @Override
    public String toString() {
        return "Human{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", educationLevel='" + educationLevel + '\'' +
                '}';
    }

    public String action(String actionType) {
        return actionType;
    }

    public boolean isHumanAllowedToVote(int age) {
        boolean flag;
        switch (age) {
            case 10:
            case 17: {
                flag = false;
                break;
            }
            case 25: {
                flag = true;
            }
            break;
            default: {
                throw new IllegalStateException("Unexpected Value: " + age);
            }
        }
        return flag;

    }
}
