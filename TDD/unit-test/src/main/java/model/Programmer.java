package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Programmer extends Human {
    private String technologyStack;
    private double baseSalary = 2500;
    private double salaryIncreasePercentage;
    private int pensionAge = 70;
    private int annualSalaryIncreasePercentage = 10;


    @Override
    public String toString() {
        return "Programmer{" +
                "technologyStack='" + technologyStack + '\'' +
                ", salary=" + baseSalary +
                '}';
    }

    public double increaseSalary(double salaryIncreasePercentage) {
        return baseSalary + (baseSalary * salaryIncreasePercentage) / 100;
    }

    public double calculateCashBalanceUntilRetirement() {
        double annualSalary = baseSalary * 12;
        double totalSalary = 0;
        for (int i = 20; i <= 22; i++) {
            totalSalary = annualSalary + (annualSalary * salaryIncreasePercentage) / 100;
        }
        return totalSalary;
    }
}
