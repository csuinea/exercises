import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MyTest {

    @BeforeAll
    public static void beforeAllTests() {
        System.out.println("This is before all!");
    }

    @BeforeEach
    public void beforeTest() {
        System.out.println("This is before Test");
    }

    @Test
    @Order(2)
    public void test1() {
        System.out.println("This is text for test1");
    }

    @Test
    @Order(1)
    public void test2() {
        System.out.println("This is text for test2");
    }

    @AfterEach
    public void afterTest() {
        System.out.println("This is after Test");
    }

    @AfterAll
    public static void afterAllTests() {
        System.out.println("This is after all!");
    }

}
