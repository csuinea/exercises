import model.Human;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class HumanTest {
    Human human = new Human();

    @Test
    public void checkSetFirstNameFunctionality() {
        human.setFirstName("Mike");
        String theFirstName = human.getFirstName();
        Assertions.assertEquals("Mike", theFirstName);
    }

    @Test
    public void checkSetLastNameFunctionality() {
        human.setLastName("Mikkelsen");
        String theLastName = human.getLastName();
        Assertions.assertEquals("Mikkelsen", theLastName);
    }

    @Test
    public void checkSetAgeFunctionality() {
        human.setAge(22);
        int age = human.getAge();
        Assertions.assertEquals(22, age);

    }

    @Test
    public void checkSetEducationLevelFunctionality() {
        human.setEducationLevel("College");
        String educationalLevel = human.getEducationLevel();
        Assertions.assertEquals("College", educationalLevel);
    }

    @Test
    public void checkIfAllowedToVote() {
        boolean isAllowedToVote = human.isHumanAllowedToVote(25);
        assertTrue(isAllowedToVote);
    }
}
