import model.Doctor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DoctorTest {
    Doctor doctor = new Doctor();

    @Test
    public void checkSetFirstNameFunctionality() {
        doctor.setFirstName("Henry");
        String theFirstName = doctor.getFirstName();
        assertEquals("Henry", theFirstName);
    }

    @Test
    public void checkSetLastNameFunctionality() {
        doctor.setLastName("Deaver");
        String theLastName = doctor.getLastName();
        assertEquals("Deaver", theLastName);
    }

    @Test
    public void checkSetAgeFunctionality() {
        doctor.setAge(36);
        int age = doctor.getAge();
        assertEquals(36, age);

    }

    @Test
    public void checkSetEducationLevelFunctionality() {
        doctor.setEducationLevel("PhD");
        String educationalLevel = doctor.getEducationLevel();
        assertEquals("PhD", educationalLevel);
    }

    @Test
    public void checkSetSpecialtyFunctionality() {
        doctor.setSpeciality("Neurologist");
        String speciality = doctor.getSpeciality();
        assertEquals("Neurologist", speciality);
    }

    @Test
    public void checkCalculateTotalIncomeFunctionality() {
        doctor.setSalary(1000);
        doctor.setBribe(50.5);

        double income = doctor.calculateTotalIncome();

        assertEquals(income, 1050.5);
    }

    @Test
    public void checkBribeLevel() {
        doctor.setBribe(655);

        boolean isFraudulent = doctor.checkBribeLevel();

        assertFalse(isFraudulent);
    }

    @Test
    public void calculateProficiencyLevel1() {
        doctor.setNumberOfCredits(5);

        String level = doctor.calculateProficiencyLevel();

        assertEquals("L1", level);

    }

    @Test
    public void calculateProficiencyLevel2() {
        doctor.setNumberOfCredits(12);

        String level = doctor.calculateProficiencyLevel();

        assertEquals("L2", level);

    }

    @Test
    public void calculateProficiencyLevel3() {
        doctor.setNumberOfCredits(62);

        String level = doctor.calculateProficiencyLevel();

        assertEquals("L3", level);

    }

}
