import model.Programmer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProgrammerTest {
    Programmer programmer = new Programmer();

    @Test
    public void checkSetFirstNameFunctionality() {
        programmer.setFirstName("Julius");
        String theFirstName = programmer.getFirstName();
        assertEquals("Henry", theFirstName);
    }

    @Test
    public void checkSetLastNameFunctionality() {
        programmer.setLastName("Caesar");
        String theLastName = programmer.getLastName();
        assertEquals("Deaver", theLastName);
    }

    @Test
    public void checkSetAgeFunctionality() {
        programmer.setAge(45);
        int age = programmer.getAge();
        assertEquals(45, age);
    }

    @Test
    public void checkSetEducationLevelFunctionality() {
        programmer.setEducationLevel("Highschool");
        String educationalLevel = programmer.getEducationLevel();
        assertEquals("Highschool", educationalLevel);
    }

    @Test
    public void checkSetTechnologyStackFunctionality() {
        programmer.setTechnologyStack("Javascript");
        String techStack = programmer.getTechnologyStack();
        Assertions.assertNotEquals("Test", techStack);
    }

    @Test
    public void checkSetSalaryFunctionality() {
        programmer.setBaseSalary(1600);
        double salary = programmer.getBaseSalary();
        Assertions.assertNotNull(salary);

    }

    @Test
    public void checkSalaryIncrease() {
        double newSalary = programmer.increaseSalary(10);
        assertEquals(2750, newSalary);
    }

    @Test
    public void checkTotalIncome() {
        double salary = programmer.calculateCashBalanceUntilRetirement();
        System.out.println(salary);
    }

}
